package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException{
		System.out.println("*****************************************");
		System.out.println("CalculatorServlet initialized successfully");
		System.out.println("*****************************************");
	}
	
	public void doGet (HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the Calculator App</h1>");
		out.println("To use the app, input two numbers and an operation<br><br>");
		out.println("Hit the submit button after filling in the details.<br><br>");
		out.println("You will get the results shown in your browser!");
	}
	public void doPost (HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String op = req.getParameter("op").toLowerCase();
		int total = 0;
		boolean valid = true;
		
		switch(op) {
		
		case "add":
			total = num1 + num2;
			break;
		
		case "subtract":
			total = num1 - num2;
			break;
		
		case "multiply":
			total = num1 * num2;
			break;
		
		case "divide":
			total = num1 / num2;
			break;
		
		default: 
			valid = false;
			out.println("Invalid operation entered!");
		}
		
		out.println("The two numbers you provided are: " + num1 + ", " + num2);
		out.println("The operation that you wanted is: " + op);
		if(valid) {
		out.println("The result is: " + total);
		}
		
	}
	
	public void destroy(){
		System.out.println("*****************************************");
		System.out.println("CalculatorServlet destroyed successfully");
		System.out.println("*****************************************");
	}
}
